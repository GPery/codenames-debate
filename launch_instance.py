import argparse
import itertools
import logging
import os
import time

import inquirer
import plyer
import requests

LL_BASE_URL = "https://cloud.lambdalabs.com/api/v1"

logging.basicConfig(format="%(levelname)s:%(message)s", level=logging.INFO)


def main():
    ap = argparse.ArgumentParser()
    ap.add_argument(
        "-t",
        "--token",
        help="Lambda Labs API token (default: LL_API_TOKEN environment variable)",
    )
    ap.add_argument(
        "--max-price",
        help="Maximum price for instance in cents per hour",
        default=100,
        type=int,
    )

    args = ap.parse_args()
    token = args.token or os.environ["LL_API_TOKEN"]
    auth = requests.auth.HTTPBasicAuth(token, "")
    session = requests.Session()
    session.auth = auth

    # Instances
    for i in itertools.count(start=1):
        r = session.get(LL_BASE_URL + "/instance-types")
        r.raise_for_status()
        instances = r.json()["data"]
        available_instances = {
            k: v
            for k, v in instances.items()
            if len(v["regions_with_capacity_available"]) > 0
        }
        matched_instances = {
            k: v
            for k, v in available_instances.items()
            if v["instance_type"]["price_cents_per_hour"] <= args.max_price
        }

        if len(matched_instances) > 0:
            if i > 5:
                plyer.notification.notify("Machine found!")
            break
        if i % 5 == 0:
            logging.info(
                f"Attempting to pull instances (attempt #{i}, {len(available_instances)} instances dropped by filter)..."
            )
        if i == 5:
            logging.info("You will be notified when a machine is found.")

        time.sleep(2)

    # Filesystems
    r = session.get(LL_BASE_URL + "/file-systems")
    r.raise_for_status()
    filesystems = r.json()["data"]

    filesystems_available = {
        k: [
            fs
            for fs in filesystems
            if fs["region"]["name"]
            in map(
                lambda region: region["name"],
                matched_instances[k]["regions_with_capacity_available"],
            )
        ]
        for k in matched_instances.keys()
    }
    options = [
        f"{key}: {i['instance_type']['description'].rstrip(')')}, \
{i['instance_type']['price_cents_per_hour']}¢/h) (filesystems available: {', '.join(map(lambda fs: fs['name'], filesystems_available[key])) if filesystems_available[key] else 'none'})"
        for key, i in matched_instances.items()
    ]
    instance = inquirer.list_input("Choose instance", choices=options).split(":")[0]
    regions = instances[instance]["regions_with_capacity_available"]
    fs_per_region = {
        rg["name"]: [
            fs
            for fs in filesystems_available[instance]
            if fs["region"]["name"] == rg["name"]
        ]
        for rg in regions
    }
    region_options = [
        f"{rg['name']} (filesystems available: {', '.join(map(lambda fs: fs['name'], fs_per_region[rg['name']]))})"
        for rg in regions
    ]
    region = inquirer.list_input("Choose region", choices=region_options).split()[0]
    region_filesystems = fs_per_region[region]

    filesystem_options = [
        f"{fs['name']} (created by {fs['created_by']['email']}{', is in use' if fs['is_in_use'] else ''})"
        for fs in region_filesystems
    ] + ["No filesystem"]
    filesystem_choice = inquirer.list_input(
        "Choose filesystem", choices=filesystem_options
    )
    if filesystem_choice != "No filesystem":
        fs_name = filesystem_choice.split()[0]
        filesystem = next(filter(lambda fs: fs["name"] == fs_name, region_filesystems))
    else:
        filesystem = None

    # SSH key
    r = session.get(LL_BASE_URL + "/ssh-keys")
    r.raise_for_status()
    ssh_keys = r.json()["data"]
    ssh_choice = inquirer.list_input(
        "Choose SSH key", choices=[k["name"] for k in ssh_keys]
    )

    launch_request = {
        "region_name": region,
        "instance_type_name": instance,
        "ssh_key_names": [ssh_choice],  # Multiple keys are not supported
    }
    if filesystem is not None:
        launch_request["file_system_names"] = [
            filesystem["name"]
        ]  # Multiple FS are not supported

    r = session.post(LL_BASE_URL + "/instance-operations/launch", json=launch_request)
    r.raise_for_status()
    instance_id = r.json()["data"]["instance_ids"][0]

    while True:
        r = session.get(LL_BASE_URL + f"/instances/{instance_id}")
        r.raise_for_status()
        instance_data = r.json()["data"]
        if "ip" in instance_data and "jupyter_url" in instance_data:
            break
        time.sleep(1)
    print(
        f"Instance up.\nIP: {instance_data['ip']}\njupyter URL: {instance_data['jupyter_url']}\njupyter token: {instance_data['jupyter_token']}"
    )


if __name__ == "__main__":
    main()
